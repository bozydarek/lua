function print_tab (tab)
    io.write "[ "
    for i=1, #tab-1 do
        io.write (tab[i])
        io.write ", "
    end
    io.write (tab[#tab])
    print (" ]")
    -- print (tab[#tab] .. " ]") -- ciekawe ze print wypisuje wartosci inaczej niz io.write 
end

function prime(n)
    if n<=1 then return false end
    if n==2 then return true end
    if n%2==0 then return false end

    for i = 3, n^(1/2), 2 do
        if (n % i) == 0 then
            return false
        end
    end
    return true
end