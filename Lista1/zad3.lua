require("tools");

function dividers( num )
    tmp = {}
    div = 2

    while num ~= 1 do
        if num%div == 0 then
            tmp[#tmp+1] = div
            num = num / div
        else
            div = div + 1
            -- while not prime(div) do
            --     div = div + 1
            -- end
        end
    end

    return tmp
end

-- for i=1,10 do
--     io.write(i.." ")
--     print(prime(i))
-- end

print_tab(dividers(56))
print_tab(dividers(3456))
print_tab(dividers(84049920))
print_tab(dividers(1206200401920))
print_tab(dividers(2198137*23*5*2))