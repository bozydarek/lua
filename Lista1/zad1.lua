
function printtab(tab)
  str = ''
  for i=1,#tab do
    str = str..tab[i]..' '
  end
  print (str)
end

printtab ( {'ala', 'ma', 127, 'kotów'} )


function concat( tab )
    str = '{'
    for i, v in ipairs(tab) do
        if type(v) =='table' then
            str = str..concat(v)
        else
            str = str..tostring(tab[i])
        end
        -- add comma after object
        if i~=#tab then 
                str=str..',' 
        end
    end
    return str..'}'
end

function printf(tab)
    print(concat(tab))
end

printf ( {'ala', 'ma', 127, 'kotów'} )
printf ( { 'to są' , {} , {2 , 'tablice'} , 'zagnieżdżone?'} )
printf ( { 'to są' , {} , {2 , 'tablice'} , 'zagnieżdżone?' , { true }} )
printf ( { 'to są' , {{{}},{},{false}} , {2 , 'tablice',{}} , {}, 'zagnieżdżone?' , { true }} )