require("tools");

function map(tab, f)
    tmp = {}
    -- for i=1, #tab do
    -- tmp[i] = f (tab[i])
    for i, v in ipairs(tab) do
        tmp[i] = f (v)
    end
    return tmp
end

function inc( x )
    return x+1
end

function sq( x )
    return x^2 --x*x;
end

t = {1,2,3}
v = map(t, inc)
print_tab(v)

t2 = {9,8,7,6,5,4,3,2,1}
v2 = map(t2, sq)
print_tab(v2)
