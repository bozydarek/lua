package.path = package.path .. ";../Tools/?.lua"
require("tests")

function is_num( expr )
    return string.match( expr,"[%-]?[%d]+[%.]?[%d]*" ) == expr
end

function check_simple_expr( expr )
    local simp_expr = expr:gsub("%b()", "1" )
    local x,y = string.match( simp_expr, "(.+)[%+-*/]+(.+)$" )
    if x == nil then 
        return is_num(simp_expr)
    end
    return is_num(y) and check_simple_expr(x)
end

function check_rec_by_bracketing( expr )
    if check_simple_expr(expr) == false then 
        return false
    end

    for x in string.gmatch( expr, "%b()" ) do
        -- print(x)
        if check_rec_by_bracketing(x:sub(2,-2)) == false then 
            return false
        end
    end

    return true
end

function math_exp_check( expr )
    if string.match( expr,"%d %d" ) ~= nil then
        return false
    end

    expr = expr:gsub(" ", "")
    if string.match( expr,"[^%d%.+-/*()]" ) ~= nil then
        return false
    end

    return check_rec_by_bracketing(expr)
end


math_exp_tests = {
    "test",
    "2+2",
    "1+2+3",
    "2+(2*2)",
    " 2 +  ( 2  * 2    )   ",
    "(2*(3-1*(4-3)) / 5)+(2-4)-(43*34)",
    "2+++",
    "2++2",
    "2+-2",
    "2+(-2)",
    "*2+(-2)",
    "-2+(-2)",
    "5+7!+9",
    "(22.22+22.2)+22.22)",
    "((22.22+22.2)+22.22)",
    "((((1))))",
    "((((1))) ",
    " (((1))))",
    "- 2+ 4.503",
    "(2*3.5*4)- (-12)/3",
    "1+(+2+)+3",
    "1+(+2)+3",
    "2 - -5",
    "----6",
    "2 3",
    "(2+2)(3*3)",
    "(2+2) (3*3)",
    "(2+2 3)(1 3*3 2)"
}

for _,v in ipairs(math_exp_tests) do
    print(v, "->" ,math_exp_check(v))
end

-- other tests
num_tests = {
    {"test",false},
    {"2",true},
    {"*2",false},
    {"2+",false},
    {"(-2)",false},
    {"-2",true},
    {"2-",false},
    {"+2+",false},
    {"2.2",true},
    {"0.234893759287492387423",true},
    {"0..234",false},
    {"40.2453.454",false},
    {"22+2",false},
    {"232", true},
    {"232.2343242", true}
}

run_tests(is_num,"is_num", num_tests)

simple_expr_test = {
    {"2+2",true},
    {"2*2",true},
    {"1+2+3",true},
    {"2+2+3+4+2+4+2",true},
    {"2*2*3*4*",false},
    {"2.22/2.22",true},
    {"2.22//2.22",false},
    {"2.22.22.22+2.2",false},
    {"22.22+22.2",true},
    {"-22.22+22.2/2*2-2+2.2-2+2-2+2-2/2",true},
    {"1",true},
    {"6---66", false}
}

run_tests(check_simple_expr,"check_simple_expr",simple_expr_test)
