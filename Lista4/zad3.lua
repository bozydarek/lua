package.path = package.path .. ";../Tools/?.lua"
require("tools")

-- https://en.wikipedia.org/wiki/Filename#Comparison_of_filename_limitations
slash = package.config:sub( 1, 1 )
pattern_for_filename_and_extension = "([^"..slash.."]*)%.([%w]*)$"
pattern_for_all_elements_in_path = slash.."([^"..slash.."]+)"

function unpath( path )
    local f_name, ext = string.match( path, pattern_for_filename_and_extension )

    local comp = {}
    for w in string.gmatch(path, pattern_for_all_elements_in_path) do
        comp[#comp+1] = w
    end

    if f_name ~= nil then
        comp[#comp] = {f_name, ext}
    end

    return comp
end


-- print(package.config)

path_tests = {
        "/home/bozydar/Folder/plik.txt",
        "/home/bozydar/Folder/plik_pdf.pdf",
        "/home/bozydar/Folder/Podfolder/Jakis starasznie glupi katalog/plik.mp4",
        "/home/bozydar/plik.z.kropkami.w.nazwie.TXT",
        "/home/bozydar/Documents",
        "/home/bozydar/Documents/Gęśla_Jaźń.pdf",
        "/home/bozydar/O ja cie sune !!!/Zdjęcia/1.04.2016/WTF?!@#^%$#?!@#?*&/Dziwny Obiekt latający.JPEG" }

for _,v in ipairs(path_tests) do
    print_tab_rec(unpath(v))
end
