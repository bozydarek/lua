#include "board.h"
#include <iostream>
#include <assert.h>

Board::Board()
{
    #ifdef DEBUG
        std::cout << "NEW board!\n";
    #endif

    resetBoard();
}

void Board::resetBoard() {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            board[i][j] = Field::EMPTY;
        }
    }
}

bool Board::setField(int i, int j, Field type) {
    assert(i>=0 && i<3);
    assert(j>=0 && j<3);

    if( board[i][j] == Field::EMPTY )
    {
        board[i][j] = type;
        return true;
    }

    return false;
}

Field Board::getField(int i, int j) {
    assert(i>=0 && i<3);
    assert(j>=0 && j<3);

    return board[i][j];
}


std::string fieldSymbol(Field f)
{
    switch (f)
    {
        case EMPTY:
            return " ";
        case O:
            return "O";
        case X:
            return "X";
    }

    assert(false);
    return "E";
}


std::string Board::getFieldS(int i, int j) {

    return fieldSymbol(getField(i,j));
}


void Board::print() {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            std::cout << fieldSymbol(board[i][j]) << (j==2 ? "\n" : "|");
        }
        std::cout << (i==2 ? "" : "-----\n");
    }

}

bool Board::checkBounds(std::pair<int, int> move) {
    int i = move.first;
    int j = move.second;

    return  i>=0 && i<3 && j>=0 && j<3;
}

bool Board::setField(std::pair<int, int> move, Field type) {
    return setField(move.first, move.second, type);
}

Field Board::getField(std::pair<int, int> move) {
    return getField(move.first, move.second);
}

