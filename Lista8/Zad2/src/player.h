#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <lua.hpp>
#include "board.h"

class Player {
    lua_State*  luaState;
    Field       symbol;

    std::string fileName;
public:
    Player(std::string luaFile, Field symbol_);
    ~Player();

    std::pair<int, int> callAI(std::unique_ptr<Board> &board);
    std::string         getName();
    Field getSymbol() const;
};


#endif //PLAYER_H
