#ifndef GAME_H
#define GAME_H

#include <memory>
#include "board.h"
#include "player.h"

enum GameStatus{
    Ready,
    InProgress,
    Draw,
    WinO,
    WinX
};

enum Round{
    play_O,
    play_X
};

class Game {
    std::unique_ptr<Board>  board = nullptr;
    GameStatus              status = GameStatus::Ready;
    Round                   round = Round::play_O;
    unsigned int            roundNo = 0;

    std::shared_ptr<Player> playerO, playerX;

    std::string             errorMsg = "No error yet.";

public:
    Game(std::shared_ptr<Player> pO, std::shared_ptr<Player> pX);

    void newGame();
    bool end();
    bool nextMove();

    std::string getLastError();

    void printState();
    std::string getResult();

    int getReturnCode();

private:
    void checkForPosibleWiner();
    void markWinner(Field field);
};


#endif //GAME_H
