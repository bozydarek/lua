#include <assert.h>
#include <iostream>
#include "game.h"

Game::Game(std::shared_ptr<Player> pO, std::shared_ptr<Player> pX)
    :playerO(pO)
    ,playerX(pX)
{
    board = std::make_unique<Board>();
}


void Game::newGame() {
    status = GameStatus::InProgress;
    roundNo = 0;
}

bool Game::end()
{
    return status != GameStatus::InProgress;
}

bool Game::nextMove()
{
    assert(not end());

    std::shared_ptr<Player> player = nullptr;
    if(round == Round::play_O)
    {
        player = playerO;
        round = Round::play_X;
    }
    else
    {
        player = playerX;
        round = Round::play_O;
    }

    std::pair<int,int> move = player->callAI(board);

    if(board->checkBounds(move))
    {
        if(board->getField(move) == Field::EMPTY)
        {
            board->setField(move, player->getSymbol());
            ++roundNo;

            //std::cout << "R:" << roundNo << "\n";
            if(roundNo >= 5)
            {
                checkForPosibleWiner();
            }

            return true;
        }
        else
        {
            errorMsg = player->getName() + " made illegal move: (" + std::to_string(move.first) + "," + std::to_string(move.second) + ")"
                       + " is already taken";
        }
    }
    else
    {
        errorMsg = player->getName() + " made illegal move: (" + std::to_string(move.first) + "," + std::to_string(move.second) + ")"
                    + " is beyond the borders of the board";
    }

    return false;
}

std::string Game::getLastError() {
    return errorMsg;
}

void Game::printState() {
    std::cout << "=-=-=-=-=-=" << std::endl;
    std::cout << "Move: " << roundNo << " - after player " << (round == Round::play_O ? "X" : "O") << " move" << std::endl;

    board->print();

#ifdef DEBUG
    std::cout << "Player " << (round == Round::play_O ? "O" : "X") << " turn:" << std::endl;
#endif

}

void Game::checkForPosibleWiner() {
    if(roundNo == 9)
    {
        status = GameStatus::Draw;
    }

    // check horizontal
    for (int i = 0; i < 3; ++i) {
        Field first = board->getField(i, 0);
        if(first != Field::EMPTY) {
            if(first == board->getField(i,1) && first == board->getField(i,2))
            {
                markWinner(first);
                return;
            }
        }
    }

    // check vertical
    for (int i = 0; i < 3; ++i) {
        Field first = board->getField(0, i);
        if(first != Field::EMPTY) {
            if(first == board->getField(1,i) && first == board->getField(2,i))
            {
                markWinner(first);
                return;
            }
        }
    }

    // check diagonal 1
    {
        Field first = board->getField(0, 0);
        if(first != Field::EMPTY) {
            if(first == board->getField(1,1) && first == board->getField(2,2))
            {
                markWinner(first);
                return;
            }
        }
    }

    // check diagonal 2
    {
        Field first = board->getField(0, 2);
        if(first != Field::EMPTY) {
            if(first == board->getField(1,1) && first == board->getField(2,0))
            {
                markWinner(first);
                return;
            }
        }
    }
}

std::string Game::getResult() {
    switch (status)
    {
        case Ready:
            return "Pleas start new game first.";
        case InProgress:
            return "Game is not finished yet!";
        case Draw:
            return "Draw! There is no winner.";
        case WinO:
            return "The winner is: " + playerO->getName();
        case WinX:
            return "The winner is: " + playerX->getName();
    }

    assert(false && "Any missing cases?");
    return "Error!";
}

void Game::markWinner(Field field) {
    switch (field)
    {
        case EMPTY:
            assert(false && "Wat?");
            break;
        case O:
            status = GameStatus::WinO;
            break;
        case X:
            status = GameStatus::WinX;
            break;
    }
}

int Game::getReturnCode() {
    switch (status)
    {
        case Ready:
            return -11;
        case InProgress:
            return -12;
        case Draw:
            return 0;
        case WinO:
            return 1;
        case WinX:
            return 2;
    }

    assert(false && "Any missing cases?");
    return -13;
}
