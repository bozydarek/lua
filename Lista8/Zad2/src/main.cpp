#include <iostream>
#include <string>

//#include "board.h" // for test
#include "game.h"

using namespace std;

void man()
{
    cout << "\033[1mTicTacToe\033[0m for bots in lua.\n";
    cout << "Program return:\n";
    cout << "\t 0 if draw\n";
    cout << "\t 1 if first player won\n";
    cout << "\t 2 if second player won\n";

    cout << "\033[1mUsage:\033[0m\n";
    cout << "./TicTacToe AI_bot_O.lua AI_bot_X.lua\n";
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        cout << "\033[1;31mError:\033[0m Wrong number of arguments ( "<< argc-1 <<" for 2 )\n";
        man();
        return -1;
    }

    std::shared_ptr<Player> player[2];
    Field symbol[2] = {Field::O, Field::X};

    for (int i = 0; i < 2; i++)
    {
        string argument(argv[i+1]);
        unsigned long k = argument.length();

        if (k < 4 || argument.substr(k - 4) != ".lua") {
            cout << "\033[1;31mError:\033[0m Wrong argument: \"" << argument << "\"\n";
            man();
            return -2;
        }

        player[i] = std::make_shared<Player>(argument, symbol[i]);
    }


    Game game(player[0], player[1]);

    game.newGame();

    while( not game.end() )
    {
        if( not game.nextMove() )
        {
            cout << "\033[1;31mError:\033[0m " << game.getLastError() << endl;
            return -3;
        }
        game.printState();
    }

    cout << "\n\033[1mGame over.\033[0m\n" << game.getResult() << endl;



    return game.getReturnCode();
}