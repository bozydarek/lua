#ifndef BOARD_H
#define BOARD_H

#include <string>

enum Field{
    EMPTY,
    O,
    X,
};

std::string fieldSymbol(Field f);


class Board{
    Field board[3][3];
public:
    Board();

    bool    setField(int i, int j, Field type);
    bool    setField(std::pair<int, int> move, Field type);
    Field   getField(int i, int j);
    Field   getField(std::pair<int, int> move);
    std::string getFieldS(int i, int j);

    void    print();
    bool    checkBounds(std::pair<int, int> move);

private:
    void resetBoard();
};

#endif // BOARD_H