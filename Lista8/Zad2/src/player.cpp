#include <assert.h>
#include <bits/unique_ptr.h>
#include "player.h"
#include "tools.h"

Player::Player(std::string luaFile, Field symbol_)
    :symbol(symbol_)
{
    luaState = luaL_newstate();
    luaL_openlibs(luaState);

    if (luaL_loadfile(luaState, luaFile.c_str())  || lua_pcall(luaState, 0, 0, 0)) {
        print_error(luaState, "\033[1;31mError:\033[0m Cannot load config file: %s\n", lua_tostring(luaState, -1));
    }

    lua_getglobal(luaState, "AI");
    if( not lua_isfunction(luaState, lua_gettop(luaState)) ) {
        print_error(luaState, "\033[1;31mError:\033[0m Function AI not found in %s\n", luaFile.c_str());
    }

    // Take file name and remove extension and path (is occurs)
    fileName = luaFile.substr(0, luaFile.length()-4);

    std::size_t found = fileName.rfind("/");

    if (found != std::string::npos) {
        fileName = fileName.substr(found+1);
    }

}

Player::~Player() {
    lua_close(luaState);
}

std::pair<int, int> Player::callAI(std::unique_ptr<Board> &board) {
    lua_getglobal(luaState, "AI");

    // send player symbol
    lua_pushstring(luaState, fieldSymbol(symbol).c_str());

    // send board
    lua_createtable(luaState, 3, 0);

    for (int i = 0; i < 3; ++i) {
        lua_pushnumber(luaState, i+1);
        lua_createtable(luaState, 0, 3);

        for (int j = 0; j < 3; ++j) {
            lua_pushstring(luaState, board->getFieldS(i,j).c_str());
            lua_seti(luaState, -2, (j+1));
        }
        
        lua_settable(luaState, -3);
    }

    int res = lua_pcall(luaState, 2, 2, 0);

    if( res != LUA_OK )
    {
        std::cout << "\033[1;31mError:\033[0m " << res <<":\n";
        print_error(luaState, "%s\n", lua_tostring(luaState, -1));

        assert(false && "Pcall error!");
    }

    int x,y;

    x = (int)lua_tointeger(luaState, -2) - 1; // this -1 is because lua is 1-based and C/C++ are 0-based
    y = (int)lua_tointeger(luaState, -1) - 1; // same as above

    lua_pop(luaState, 2);

    #ifdef DEBUG
        std::cout << getName() << " -> X:" << x << " Y:" << y << "\n";
    #endif

    return std::make_pair(x, y);
}

std::string Player::getName() {
    return "Player "+fieldSymbol(symbol) + " (" + fileName + ")";
}

Field Player::getSymbol() const {
    return symbol;
}
