local function rng () 
    return math.random (3) 
end

math.randomseed( os.time() )


local function round(board)
    local roundNo = 0
    for i=1,3 do
        for j=1,3 do
            if board[i][j] ~= ' ' then
                roundNo = roundNo + 1
            end
        end
    end
    return roundNo
end

local function winOrBlock(mysymbol, board)
    -- horizontal 
    for i=1,3 do
        local ratio = {X = 0, O = 0}
        for j=1,3 do
            if board[i][j] ~= ' ' then
                ratio[board[i][j]] = ratio[board[i][j]] + 1
            end
        end
        if ratio["X"] + ratio["O"] == 2 and (ratio["X"] == 2 or ratio["O"] == 2) then
            for j=1,3 do
                if board[i][j] == ' ' then
                    return i, j
                end
            end
        end
    end

    -- vertical 
    for j=1,3 do
        local ratio = {X = 0, O = 0}
        for i=1,3 do
            if board[i][j] ~= ' ' then
                ratio[board[i][j]] = ratio[board[i][j]] + 1
            end
        end
        if ratio["X"] + ratio["O"] == 2 and (ratio["X"] == 2 or ratio["O"] == 2) then
            for i=1,3 do
                if board[i][j] == ' ' then
                    return i, j
                end
            end
        end
    end

    --diagonal 1
    local ratio = {X = 0, O = 0}
    for i=1,3 do
        if board[i][i] ~= ' ' then
            ratio[board[i][i]] = ratio[board[i][i]] + 1
        end
    end
    if ratio["X"] + ratio["O"] == 2 and (ratio["X"] == 2 or ratio["O"] == 2) then
        for i=1,3 do
            if board[i][i] == ' ' then
                return i, i
            end
        end
    end
    ratio = nil

    --diagonal 2
    local ratio = {X = 0, O = 0}
    for i=1,3 do
        if board[4-i][i] ~= ' ' then
            ratio[board[4-i][i]] = ratio[board[4-i][i]] + 1
        end
    end
    if ratio["X"] + ratio["O"] == 2 and (ratio["X"] == 2 or ratio["O"] == 2) then
        for i=1,3 do
            if board[4-i][i] == ' ' then
                return 4-i, i
            end
        end
    end
    ratio = nil


    -- make random move
    while true do
        local x, y = rng(), rng()
        if board [x][y] == ' ' then
            return x, y
        end
    end

end


AI = function ( mysymbol , board )
    local r = round(board)
    if r == 0 then
        return 2, 2
    elseif r == 1 then
        if board[2][2] == ' ' then
            return 2,2
        else
            return 1,1
        end
    end

    while true do
        return winOrBlock(mysymbol, board)
    end
end

-- print ( AI ('X', {{'X', ' ', ' '} , {'X',' ', 'O'} , {'O', ' ','O'}}))
-- print ( AI ('X', {  {' ', 'O', ' '} , 
--                     {' ', 'O', ' '} , 
--                     {'X', ' ', ' '}  }))