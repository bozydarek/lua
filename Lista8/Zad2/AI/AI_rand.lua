local function rng () 
    return math.random (3) 
end

local function print_board( board)
    print(board[1][1],board[1][2],board[1][3])
    print(board[2][1],board[2][2],board[2][3])
    print(board[3][1],board[3][2],board[3][3])
end

AI = function ( mysymbol , board )
--    print_board( board );
    while true do
        local x, y = rng(), rng()
        if board [x][y] == ' ' then
            return x, y
        end
    end
end

-- print ( AI ('X', {{'X', ' ', ' '} , {' ',' ', 'O '} , {'O', ' ','X'}}))
-- print ( AI ('X', {{'X', ' ', ' '} , {' ',' ', 'O '} , {'O', ' ','X'}}))
-- print ( AI ('X', {{'X', ' ', ' '} , {' ',' ', 'O '} , {'O', ' ','X'}}))
-- print ( AI ('X', {{'X', ' ', ' '} , {' ',' ', 'O '} , {'O', ' ','X'}}))
-- print ( AI ('X', {{'X', ' ', ' '} , {' ',' ', 'O '} , {'O', ' ','X'}}))