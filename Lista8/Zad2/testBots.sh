#!/bin/bash

if [[ $# -eq 3 ]]; then
    echo $'\033[1mTest\033[0m' $1 $' \033[1mvs.\033[0m ' $2 ' -> in ' $((2*$3)) ' tests'

    player1=0
    player2=0
    draw=0
    errors=0

    for ((i=1; i<=$3; i++)); do
        ./build/TicTacToe $1 $2 &>/dev/null;
        res=$?

        if (($res == 0)); then
            ((draw++))
        elif (($res == 1)); then
            ((player1++))
        elif (($res == 2)); then
            ((player2++))
        else 
            ((errors++))
        fi
    done

    for ((i=1; i<=$3; i++)); do
        ./build/TicTacToe $2 $1 &>/dev/null;
        res=$?

        if (($res == 0)); then
            ((draw++))
        elif (($res == 1)); then
            ((player2++))
        elif (($res == 2)); then
            ((player1++))
        else 
            ((errors++))
        fi
    done

    echo $1 'won' $player1 'times => ' $((100 * $player1 / ($3*2))) '%'
    echo $2 'won' $player2 'times => ' $((100 * $player2 / ($3*2))) '%'
    echo 'There was' $draw 'draws => ' $((100 * $draw / ($3*2))) '%'
    echo 'There was' $errors 'errors => ' $((100 * $errors / ($3*2))) '%'

else
    echo  $'\033[1;31mError:\033[0;1m Wrong number of arguments \033[0m';
    echo  $'\033[1mUsage:\033[0m'
    echo  $0 'bot_1.lua' 'bot_2.lua' '<NUMBER OF TESTS/2>'
    exit 2
fi