#include<iostream>
#include <cstring>
#include <csignal>
#include "Levels.h"

using namespace std;

void man()
{
    cout << "\033[1mLevelViewer\033[0m for levels in lua.\n";

    cout << "\033[1mUsage:\033[0m\n";
    cout << "./LevelViewer file_with_levels.lua\n";
}

void interrupt(int sig)
{
    cout << "\033[31mProgram interrupted (SIG: " << sig <<")\033[0m \n";
    exit(sig);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        cout << "\033[1;31mError:\033[0m Wrong number of arguments ( "<< argc-1 <<" for 1 )\n";
        man();
        return -1;
    }

    string argument(argv[1]);
    unsigned long k = argument.length();

    if (k < 4 || argument.substr(k - 4) != ".lua") {
        cout << "\033[1;31mError:\033[0m Wrong argument: \"" << argument << "\"\n";
        man();
        return -2;
    }

    Levels levels(argument);

    signal(SIGINT, interrupt);

    cout << "\033[1mLevelViewer\033[0m <> Write levels names below (or *COMMANDS):\n";

    bool end = false;

    while( not end )
    {
        char input[256];
        memset(input, 0, sizeof(input));
        if(not cin.getline(input, 256))
        {
            interrupt(SIGQUIT);
        }

        if(strlen(input) == 0)
        {
            #ifdef DEBUG
                    cout << "\033[35mNo input.\033[0m \n";
            #endif
            continue;
        }

        #ifdef DEBUG
            cout << input << "\n";
        #endif

        ParseState state = parseAndPrint(input, levels);

        switch (state)
        {
            case OK:
                #ifdef DEBUG
                    cout << "Parsed without any problem\n";
                #endif
                break;

            case Quit:
                end = true;
                break;

            case Error:
                cout << "\033[35mParse error \033[0m \n";
                break;

            case LevelError:
                cout << "\033[35mParse error: \033[0m " << levels.getLastError() << "\n";
                std::cout << "Type *NAME for display name list of all levels\n";
                break;
        }
    }

return 0;
}
