#ifndef LEVELS_H
#define LEVELS_H

#include <string>
#include <lua.hpp>
#include <vector>

enum ParseState{
    OK,
    Quit,
    Error,
    LevelError
};

class Levels {
    lua_State*                  luaState;
    std::vector<std::string>    levels;
    std::string                 errorMsg = "No error";

public:
    Levels(std::string luaFile);
    ~Levels();

    void printAll();
    void printNames();
    ParseState printOne(std::string name);

    std::string getLastError();

private:
    void printLevel(std::string name);
};


ParseState parseAndPrint(char *input, Levels &levels);

#endif //LEVELS_H
