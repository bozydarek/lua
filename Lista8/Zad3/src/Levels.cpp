#include "Levels.h"
#include "tools.h"

#include <unordered_set>
#include <algorithm>
#include <sstream>
#include <iterator>

Levels::Levels(std::string luaFile)
{
    luaState = luaL_newstate();
    luaL_openlibs(luaState);

    //=========================================
    // make blacklist of globals // (not necessary)
    std::unordered_set<std::string> blacklist;

    lua_pushglobaltable(luaState);
    lua_pushnil(luaState);
    while (lua_next(luaState,-2) != 0) {
        std::string name = lua_tostring(luaState,-2);
        blacklist.insert(name);
        // std::cout << name << "\n";
        lua_pop(luaState,1);
    }
    lua_pop(luaState,1);
    //=========================================

    if (luaL_loadfile(luaState, luaFile.c_str())  || lua_pcall(luaState, 0, 0, 0)) {
        print_error(luaState, "\033[1;31mError:\033[0m Cannot load config file: %s\n", lua_tostring(luaState, -1));
    }

    //=========================================
    // make list of globals (exclude this from black list [if exist]) - hash table ?
    lua_pushglobaltable(luaState);
    lua_pushnil(luaState);
    while (lua_next(luaState,-2) != 0) {
        std::string name = lua_tostring(luaState,-2);
        auto result = blacklist.find(name);
        if( result == blacklist.end() )
        {
            if(name.substr(0,6) == "level_")
            {
                #ifdef DEBUG
                    //stackDump(luaState, ";  ");
                    std::cout << "level_" << name.substr(6) << "\n";
                #endif

                if( lua_istable(luaState, -1) )
                {
                    lua_pushnil(luaState);
                    lua_next(luaState,-2);

                    if( lua_istable(luaState, -1) ) {
                        levels.push_back(name.substr(6));
                    }
                    else{
                        std::cout << "\033[1;35mWarning: \033[0m" << name << " is not a 2D table\n";
                    }

                    lua_pop(luaState,2);
                }
                else{
                    std::cout << "\033[1;35mWarning: \033[0m" << name << " is not a table\n";
                }
            }
        }
        lua_pop(luaState,1);
    }
    lua_pop(luaState,1);
    //=========================================

}

Levels::~Levels() {
    lua_close(luaState);
}

ParseState Levels::printOne(std::string name) {
    // find on  list and print
    if ( std::find(levels.begin(), levels.end(), name) != levels.end() )
    {
        printLevel(name);
        return ParseState::OK;
    }

    errorMsg = "Level " + name + " not found on list.";
    return ParseState::Error;
}

void Levels::printNames() {
    std::cout << "List of levels: \n";
    for (auto &&level : levels) {
        std::cout << "<> " << level << "\n";
    }
}

void Levels::printAll() {
    for (auto &&level : levels) {
        printLevel(level);
    }
}

void Levels::printLevel(std::string name) {
    std::cout << "Level " << name << ":\n";

    lua_getglobal(luaState, ("level_"+name).c_str());
    if( not lua_istable(luaState, -1) )
    {
        std::cout << "\033[1;31mError: \033[0m\n";
        print_error(luaState, "Level %s is not a table", name.c_str());
    }

    lua_pushnil(luaState);
//    stackDump(luaState, ";  ");

    while (lua_next(luaState,-2) != 0) {

        lua_pushnil(luaState);
        while (lua_next(luaState,-2) != 0) {
            std::string value = lua_tostring(luaState,-1);
            std::cout << value << " ";
//            stackDump(luaState, ";  ");
            lua_pop(luaState, 1);
        }
        std::cout << "\n";
        lua_pop(luaState, 1);
    }
    lua_pop(luaState,1);

}

std::string Levels::getLastError() {
    return errorMsg;
}

void printHelp()
{
    std::cout << "Available COMMANDS: \n";

    std::cout << "*ALL   - print all levels\n";
    std::cout << "*NAMES - print all levels names only \n";
    std::cout << "*HELP  - print this help screen\n";
    std::cout << "*QUIT  - close the program\n";
    //std::cout << "*ALL - \n";
}

ParseState parseAndPrint(char *input, Levels &levels)
{
    std::string inputS(input);
    std::istringstream iss(inputS);

    std::vector<std::string> tokens{std::istream_iterator<std::string>{iss},
                                    std::istream_iterator<std::string>{}};

    for( auto&& t : tokens )
    {
        if( t[0] == '*' )
        {
            std::string command = t.substr(1, t.length());
            if( command == "ALL" )
            {
                levels.printAll();
            }
            else if( command == "QUIT" )
            {
                return ParseState::Quit;
            }
            else if( command == "HELP" )
            {
                printHelp();
            }
            else if( command == "NAMES" )
            {
                levels.printNames();
            }
                // else if () // for other commands
            else
            {
                std::cout << "Unknown command. Type *HELP for display list of available commands\n";
                return ParseState::Error;
            }
        }
        else
        {
            if( levels.printOne(t) != ParseState::OK )
            {
                return ParseState::LevelError;
            }
        }
    }

    return ParseState::OK;
}

//bool BothAreSpaces(char lhs, char rhs)
//{
//    return (lhs == rhs) && (lhs == ' ');
//}
