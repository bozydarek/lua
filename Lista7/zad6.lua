function length(array)
    local length = 0
    for k, _ in pairs(array) do
        if type(k) == "number" then
            length = math.max(length, k)
        end
    end

    return length
end

function compareTypes(argument, properType)
    if type(properType) == "table" then
        local res = false 
        local len = length(properType)
        for i=1,len do
            res = res or compareTypes(argument, properType[i])
        end
        return res
    elseif type(properType) == "nil" then
        return true
    end
    if properType:sub(-1,-1) == "*" and properType:find(":") == nil then -- for ex. "string:%d*"
        return compareTypes(argument, {properType:sub(1,-2), 'nil'})
    end
    
    pattern = properType:match(":(.*)")

    if not (pattern == nil) then
        if type(argument) == "string" then
            return argument:match(pattern) == argument
        else
            return false
        end
    else
        if properType == "integer" then
            return math.type(argument) == properType
        elseif properType == "float" then
            return math.type(argument) == properType
        elseif properType == "bool" then
            return type(argument) == "boolean"
        else
            -- print(type(argument))
            return type(argument) == properType
        end
    end

    error("Unknown type: "..properType,0)
    return true
end

function printError(num, arg, types, what )
    if type(types) == "string" and types:find(":") ~= nil then
        pattern = types:match(":(.*)")
        error("Function call error : argument 3 is \'"..arg.."\' not a string matching "..pattern, 0)
    end

    what = what or "argument"
    local msg = "Function call error: "..what.." "..tostring(num).." is "..tostring(arg).." not "
    if type(types) == "table" then
        local len = length(types)
        for j=1, len-1 do
            msg = msg..tostring(types[j]).." or "
        end
        msg = msg..tostring(types[len])
    else
        msg = msg..tostring(types)
    end
    error(msg,0)
end

function typecheck(f, retval, ... )
    local types= {...}
    local typesno = length(types)
    
    return function(...)
        local retno = 0;
        local args = {...}

        if type(retval) == "number" then
            retno = retval;
        end

        -- check input types first
         for i=1+retno, typesno do
            --  print(i, types[i])
             if compareTypes(args[i-retno], types[i]) == false then
                printError(i-retno, args[i-retno], types[i] )
             end
         end

        -- check output types
        if retno == 0 then
            local ret = f(table.unpack(args))
            if compareTypes(ret,retval) == false then
                printError(1, ret, retval, "return value")
             end
        else
            local ret = { f(table.unpack(args)) }
            for i=1,retno do
                -- print(i, types[i])
                if compareTypes(ret[i], types[i]) == false then
                    printError(i, ret[i], types[i], "return value")
                end
            end
        end
    end
end

local fun = function (x , y )
    return x + y < 10 , x > 0 and {x , x +y , x +2* y } or print
end

local tcfun = typecheck ( fun , 
                            2 , 'bool', 'table', 
                            'integer', { 'number', 'string'} )

tcfun (10 , 20) -- > OK
tcfun (10 , '20.0') -- > OK
-- tcfun (10.0 , '20.0') -- > Function call error : argument 1 is 10.0 not an integer
-- tcfun (10 , nil ) -- > Function call error : argument 2 is nil not a number or string
-- tcfun ( -5 , 20) -- > Function call error : return value 2 is a function not a table


local tcfun2 = typecheck ( fun , 
                            2 , 'bool', 'table', 
                            'integer*', 'string:[%d]*%.5' )
-- tcfun2 (10 , '20.55')
-- tcfun2 (10.0 , '20.5')
-- tcfun2 (10.0 , '20.0')

local someF = function(a,b,c)
    return 42
end

local tcf = typecheck ( someF , 'integer', nil , 'number*', 'string:[rgb]')
tcf ({} , nil , 'r') -- > OK
-- tcf ({} , nil , 'R') -- > Function call error : argument 3 is 'R 'not a string matching [ rgb ]
tcf (127 , 23.5 , 'rgb') -- > Function call error : argument 3 is 'rgb 'not a string matching [ rgb ]