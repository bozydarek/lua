#include <iostream>
#include <string>
#include <lua.hpp>

static void stackDump (lua_State *L, std::string separator = "\n")
{
    int top = lua_gettop(L);

    std::cout << "Stack contains " << top << (top == 1 ? " element\n" : " elements.\n");

    for (int i = 1; i <= top; i++) // odwiedzamy elementy od dołu
    {
        int t = lua_type(L, i);
        int number = i; //top - i;
        switch (t)
        {
            case LUA_TSTRING: // napis
                std::cout << number << ": " << lua_tostring(L, i) << separator;
            break;

            case LUA_TBOOLEAN: // wartość logiczna
                std::cout << number << ": " << (lua_toboolean(L, i) ? "true" : "false") << separator;
            break;

            case LUA_TNUMBER: // liczby
                if (lua_isinteger(L, i)) // integer
                  std::cout << number << ": " << lua_tointeger(L, i) << separator;
                else // float
                  std::cout << number << ": " << lua_tonumber(L, i) << separator;
            break;

            default: // pozostałe
                std::cout << number << ": " << lua_typename(L, t) << separator;
            break;
        }
    }
    std::cout << std::endl;
}


int main ()
{
    lua_State *L = luaL_newstate();

    lua_pushboolean(L, 1);
    lua_pushnumber(L, 10);
    lua_pushnil(L);

    lua_pushstring(L, "hello"); stackDump(L); // true 10 nil ’hello’
    lua_pushvalue(L, -4);       stackDump(L); // true 10 nil ’hello’ true
    lua_replace(L, 3);          stackDump(L); // true 10 true ’hello’
    lua_settop(L, 6);           stackDump(L,"; \t"); // true 10 true ’hello’ nil nil
    lua_rotate(L, 3, 1);        stackDump(L,"; \t"); // true 10 nil true 'hello' nil
    lua_remove(L, -3);          stackDump(L,"; \t"); // true 10 nil 'hello' nil
    lua_settop(L, -5);          stackDump(L,"; \t"); // true

    lua_close(L);

    return 0;
}