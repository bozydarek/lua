width = 20
height = 12

speed = 5
score = 0
bonusMult = 2.5
speedMult = speed * 1.75

playerName = "Bozydar"

level = 1

if level == 1 then
    levelName = "Basic"
elseif level == 2 then
    levelName = "Normal"
elseif level == 3 then
    levelName = "Hard"
else
    levelName = "Unknown"
end

canTeleport = true -- w sumie cokolwiek poza false, nil albo brakiem tej zmiennej da true ¯\_(ツ)_/¯