#include <iostream>
#include <string>
#include <lua.hpp>


using namespace std;

void man();

void error (lua_State *L, const char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    lua_close(L);

    exit(3);
}

int getglobalint (lua_State *L, const char *varname)
{
    int isnum, result;

    lua_getglobal(L, varname); // wstawiamy zmienną globalną varname na stos
    result = (int)lua_tointegerx(L, -1, &isnum); // odczytujemy ze stosu jej wartość

    if (!isnum) // sprawdzamy czy się poprawnie wczytała
        error(L, "'%s' should be integer\n", varname);

    lua_pop(L, 1); // usuwamy wczytaną wartość ze stosu
    return result;
}

string getglobalstring (lua_State *L, const char *varname)
{
    lua_getglobal(L, varname);

    if (lua_type(L, -1) != LUA_TSTRING)
    {
        error(L, "'%s' should be string\n", varname);
    }

    string result (lua_tostring(L, -1));
    lua_pop(L, 1);
    return result;
}

bool getglobalbool (lua_State *L, const char *varname)
{
    lua_getglobal(L, varname);
    bool result (lua_toboolean(L, -1));
    lua_pop(L, 1);
    return result;
}

double getglobaldouble (lua_State *L, const char *varname)
{
    int isnum;
    double result;

    lua_getglobal(L, varname);
    result = (double)lua_tonumberx(L, -1, &isnum);

    if (!isnum)
        error(L, "'%s' should be a number\n", varname);

    lua_pop(L, 1);
    return result;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        man();
        return 1;
    }

    std::string argument(argv[1]);
    unsigned long k = argument.length();

    if (k < 4 || argument.substr(k - 4) != ".lua") {
        cout << "\033[1;31mError:\033[0m Wrong argument: \"" << argument << "\"\n";
        man();
        return 2;
    }

    lua_State *L = luaL_newstate();

    if (luaL_loadfile(L, argument.c_str())  || lua_pcall(L, 0, 0, 0)) {
        error(L, "\033[1;31mError:\033[0m Cannot load config file: %s\n", lua_tostring(L, -1));
    }

    {
        int width = getglobalint(L, "width");
        int height = getglobalint(L, "height");
        int speed = getglobalint(L, "speed");
        int score = getglobalint(L, "score");

        double bonusMult = getglobaldouble(L, "bonusMult");
        double speedMult = getglobaldouble(L, "speedMult");

        string playerName = getglobalstring(L, "playerName");

        int level = getglobalint(L, "level");
        string levelName = getglobalstring(L, "levelName");

        bool canTeleport = getglobalbool(L, "canTeleport");

        cout << "Config:\n";

        cout << "Width: " << width << "\n";
        cout << "Height: " << height << "\n";
        cout << "Speed: " << speed << "\n";
        cout << "Score: " << score << "\n";
        cout << "BonusMult: " << bonusMult << "\n";
        cout << "SpeedMult: " << speedMult << "\n";
        cout << "PlayerName: " << playerName << "\n";
        cout << "Level: " << level << "\n";
        cout << "LevelName: " << levelName << "\n";
        cout << "CanTeleport: " << (canTeleport ? "true" : "false") << "\n";


        cout << "\nTime to change something!\n\n";

        lua_pushnumber(L, score + 1500 + 100 + 900);
        lua_setglobal(L, "score");

        lua_pushnumber(L, level + 1);
        lua_setglobal(L, "level");

        lua_pushstring(L, "Barlog");
        lua_setglobal(L, "bossName");

        lua_pushboolean(L, false);
        lua_setglobal(L, "canTeleport");
    }

    {
        int width = getglobalint(L, "width");
        int height = getglobalint(L, "height");
        int speed = getglobalint(L, "speed");
        int score = getglobalint(L, "score");

        double bonusMult = getglobaldouble(L, "bonusMult");
        double speedMult = getglobaldouble(L, "speedMult");

        string playerName = getglobalstring(L, "playerName");

        int level = getglobalint(L, "level");
        string levelName = getglobalstring(L, "levelName");

        bool canTeleport = getglobalbool(L, "canTeleport");

        string bossName = getglobalstring(L, "bossName");

        cout << "Config:\n";

        cout << "Width: " << width << "\n";
        cout << "Height: " << height << "\n";
        cout << "Speed: " << speed << "\n";
        cout << "Score: " << score << "\n";
        cout << "BonusMult: " << bonusMult << "\n";
        cout << "SpeedMult: " << speedMult << "\n";
        cout << "PlayerName: " << playerName << "\n";
        cout << "BossName: " << bossName << "\n";
        cout << "Level: " << level << "\n";
        cout << "LevelName: " << levelName << "\n"; /// zapytać o to czy lua "wykonuje się tylko przy wczytaniu"
        cout << "CanTeleport: " << (canTeleport ? "true" : "false") << "\n";
    }

    return 0;
}

void man()
{
    cout << "\033[1mUsage:\033[0m\n";
    cout << "./zad2 config_file_name.lua \n";
}