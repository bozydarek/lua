utf8.normalize = function(s)
    local norm = ""
    for _, c in utf8.codes(s) do
        if c < 127 then
            norm = norm..utf8.char(c)
        end
    end
    return norm
end

print(utf8.normalize("Księżyc:\nNów"))
print(utf8.normalize("Zażółć gęślą jaźń"))

print(utf8.normalize("Pójdźże, kiń tę chmurność w głąb flaszy!"))
print(utf8.normalize("Dość gróźb fuzją, klnę, pych i małżeństw!"))
print(utf8.normalize("Pójdź w loch zbić małżeńską gęś futryn!"))
print(utf8.normalize("Filmuj rzeź żądań, pość, gnęb chłystków!"))
print(utf8.normalize("O, mógłże sęp chlań wyjść furtką bździn."))
print(utf8.normalize("Mężny bądź, chroń pułk twój i sześć flag."))
print(utf8.normalize("Chwyć małżonkę, strój bądź pleśń z fugi."))

print(utf8.normalize("© ® ™ • ½ ¼ ¾ ⅓ ⅔ † ‡ µ ¢ £ € « » ♠ ♣ ♥ ♦ ¿ �"))
print(utf8.normalize("× ÷ ± ∞ π ∅ ≤ ≥ ≠ ≈ ∧ ∨ ∩ ∪ ∈ ∀ ∃ ∄ ∑ ∏ ← ↑ → ↓ ↔ ↕ ↖ ↗ ↘ ↙ ↺ ↻ ⇒ ⇔"))