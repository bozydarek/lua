function setInput( source )
    if source ~= nil then
        local f = io.open ( source , "r" )
        if f == nil then
            print("Plik źródłowy "..source.." nie istnieje!")
            return false;
        end
        io.input(source)
    end
    return true;
end

function setOutput( target )
    if target ~= nil then
        local f = io.open ( target , "r" )
        if f ~= nil then
            print ("Plik "..target.." istnieje, czy nadpisać ? [y/N]")
            local par = io.read("l")
            if par ~= nil then
                par = par:sub( 1, 1 )

                if(par == 'y') then 
                    io.output (target)
                else
                    io.close ( f )
                end
            else
                io.close ( f )
            end
        else
            io.output (target)
        end
    end
end

function resetToSTD()
    io.input(io.stdin)
    io.output(io.stdout)
end

function lreverser(source, target)
    resetToSTD()
    setOutput(target)
    if setInput(source) == false then 
        return
    end

    local acc = {}
    repeat
        local tmp = io.read('L')
        acc[#acc+1] = tmp
    until( tmp == nil )
    io.input (): close ()
    
    for i=#acc,1,-1 do
        io.write(acc[i])
    end

    io.output(): close()

    resetToSTD()
end

-- testy

lreverser("nazwanieisniejacegopliku.txt")
lreverser("text.txt")

lreverser("text.txt", "out.txt")
lreverser("text2.txt", "out.txt")

lreverser()
