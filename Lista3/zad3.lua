utf8.sub = function(s, i, j)
    local subs = ""
    j = j or utf8.len(s)
    p = 0
    for _, c in utf8.codes(s) do
        p = p + 1
        if p >= i and p<=j then
            subs = subs..utf8.char(c)
        end
    end
    return subs
end

print(utf8.sub ("Księżyc:\nNów", 5 , 10))
print(utf8.sub ("Księżyc:\nNów", 5))
print(utf8.sub ("Zażółć gęślą jaźń", 8))
print(utf8.sub ("Zażółć gęślą jaźń", 8, 12))