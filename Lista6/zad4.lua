CVector = require("CVector")


local v = CVector {'a','d','e'}
local v2 = CVector ({'a','d',nil,'e'})
-- local v3 = CVector ('a') --> error !
local v4 = CVector {}
local w = CVector ( v ) -- > w = {'a','d','e'}

-- v:tostring()
v2:printCV()
print()
w:printCV()

print ( v : empty () , CVector {}: empty ()) -- > false true
print()

v4:push_back(2)
v4:push_back(3)

v : insert (1 , {2 , 3}) -- > v = {'a', 2 , 3 ,'d','e'}
-- v : insert (1 , v4) -- > v = {'a', 2 , 3 ,'d','e'}
v:tostring()

print("\nPop back:")
v : pop_back ()-- > v = {'a', 2 , 3 ,'d'}
v:tostring()
print ("Size: ", v : size ()) -- > 4

print("\nPush back:")
v : push_back ( nil ) -- > v = {'a', 2 , 3 ,'d', nil }
v:tostring()
print ("Size: ", v : size ()) -- > 5

for i=1,5 do
    v : push_back ( 5*i )
end

print("\nErase:")
v:tostring()
v:erase(1)
v:tostring()
v:erase(2,5)
v:tostring()

print ("Size: ", v : size ()) -- > 5

-- -- -- -- -- -- -- -- -- --
print("More feature\'s:")

local tv1 = CVector{1,2,3}
local tv2 = CVector{4,5,6}


for k , v in ipairs ( tv2 ) do 
    print ( k, '->', v ) 
end

print ("Size: ", #tv1) -- > 3

print(tostring(tv1 .. tv2))

local rv = (tv1 .. tv2)
print(rv:tostring())

local vv = CVector {'a' , nil ,'c'} ..CVector {4 , 5}
print ( vv ) -- > <a , nil ,c ,4 ,5 >
print (#vv ) -- > 5
-- vv[1] = "Ala"
-- print ( vv ) -- > <a , nil ,c ,4 ,5 >
-- print (#vv ) -- > 5
-- print ( vv [0] , vv [# vv -1]) -- > a 5
for k , v in ipairs ( vv ) do 
    print ( k, '->', v ) 
end
-- > 0 -> a
-- > 1 -> nil
-- > 2 -> c
-- > 3 -> 4
-- > 4 -> 5