Frac = require("fractions")

f = Frac(2, 3)
print(f, Frac.tofloat(f))

f2 = Frac.new(4,3)
print(f2.." = "..Frac.tofloat(f2))

print( f == f2 )
print( f == Frac(2, 3) )
f3 = f + f2
print( Frac.tofloat(f + f2) )
print( f + f2 )
print( f2 - f )
print( f2 - f2 )
print( f * f2 )

f3 = Frac.new(8,12)
print("8/12 = "..f3)
print("-(8/12) = "..-f3)
print(Frac.tofloat(f3))

print( 'Wynik: '..(Frac (2 , 3) + Frac (3 , 4)) )
print( Frac.tofloat ( Frac (5 ,3) / Frac (2 ,3))) -- > 2 .5
print( Frac.tofloat ( Frac (2 ,3) * Frac (3 ,4))) -- > 0 .5
print( Frac (2 ,3) < Frac (3 ,4)) -- > true
print( Frac (2 , 3) == Frac (8 ,12)) -- > true

print( Frac (2 ,3) > Frac (3 ,4)) -- > false
print( Frac (2 ,3) <= Frac (2 ,3)) -- > true
print( Frac (2 ,3) < Frac (2 ,3)) -- > false


print( Frac.tofloat ( Frac (5 ,3) * Frac (0 ,12))) 
-- print( Frac.tofloat ( Frac (5 ,3) / Frac (0 ,12))) -- ERROR - DIV/0!

--------------------------------------------------------
print("Mix numbers and fraction test:")
f1 = Frac(2, 3)
f2 = Frac(8, 7)

print(2 + f1)
print(2 - f1)
print(f2 - 1)
print(f2 / 8)
print(f1 * 6)

print("Mix float and fraction test:")
print(2.75 + Frac(1,4))
print(4.55 - Frac(1, 2))
print(Frac(2, 3) + 2.5 )
print(Frac(2, 3) + 0.333333333333)
print(Frac(1, 1) * math.pi )

print("Pow test:")
print( Frac(2, 3)^3)
print( Frac(2, 3)^0)
print( Frac(2, 3)^(-3))
f3 = Frac(4, 7)
print( f3^5 == Frac.inverse(f3)^(-5) )

print( f3^f3 )