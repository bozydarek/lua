local Frac = {}

-- Frac.__index = Frac

local metatab = {
    __call = function (frac, ...)
        return frac.new(...)
    end,
     __unm = function (frac)
        return Frac.unm(frac)
    end,
     __add = function (lfrac, rfrac)
        return Frac.mathOp(lfrac, rfrac, Frac.add)
    end,
     __sub = function (lfrac, rfrac)
        return Frac.mathOp(lfrac, rfrac, Frac.sub)
    end,
     __mul = function (lfrac, rfrac)
        return Frac.mathOp(lfrac, rfrac, Frac.mul)
    end,
     __div = function (lfrac, rfrac)
        return Frac.mathOp(lfrac, rfrac, Frac.div)
    end,
     __pow = function (frac, exp)
        return Frac.pow(frac, exp)
    end,
    __eq = function (lfrac, rfrac)
        return Frac.eq(lfrac, rfrac)
    end,
    __lt = function (lfrac, rfrac)
        return Frac.lt(lfrac, rfrac)
    end,
    __le = function (lfrac, rfrac)
        return Frac.le(lfrac, rfrac)
    end,
    __tostring = function (frac)
        return Frac.tostring(frac)
    end,
    __concat = function (lfrac, rfrac)
        return Frac.concat(lfrac, rfrac)
    end,
}

setmetatable(Frac, metatab)


-- local function gcd(a, b)
--     while a ~= b do
--         if a > b then
--            a = a - b; 
--         else
--            b = b - a; 
--         end
--     end
--     return a;
-- end

local function gcd(a, b)
    while b ~= 0 do
        a,b = b, a % b 
    end
    return a;
end

local function lcm(a,b)
    return math.abs(a*b) / gcd(a,b)
end

local function normalize(frac)
    local a = frac.numerator
    local b = frac.denominator

    if a == 0 then 
        frac.denominator = 1
        return 
    end

    if b == 0 then
        error("DIV/0!", 0) --2)
    end

    local neqative_sign = false

    if a<0 or b<0 then
        if a<0 and b<0 then
            a = -a
            b = -b
        else
            neqative_sign = true
            a = math.abs(a)
            b = math.abs(b)
        end
    end

    local div = gcd(a,b)

    if neqative_sign then a = -a end

    frac.numerator = a / div;
    frac.denominator = b / div;
end

function Frac.new(a, b)
    local self = setmetatable({}, metatab)
    -- print("constructor->"..tostring(a).."/"..tostring(b))
    self.numerator = a
    self.denominator = b
    normalize(self)
    return self
end

function Frac.tostring(frac)
    normalize(frac)

    local a = frac.numerator
    local b = frac.denominator

    if a == 0 then 
        return "0" 
    end

    if b == 1 then
        return tostring(math.floor(a))
    end

    if a>b then
        local c = math.floor( a / b )
        return tostring(c).." "..tostring(math.floor(a-b*c)).."/"..tostring(math.floor(b))
    end

    return tostring(math.floor(a)).."/"..tostring(math.floor(b))
end

function Frac.concat(lobj, robj)
    if type(lobj)=="string" then
        return lobj..tostring(robj)
    elseif type(robj)=="string" then
        return tostring(lobj)..robj
    end
    return tostring(lobj)..tostring(robj)
end

function Frac.tofloat(frac)
    return frac.numerator / frac.denominator
end

function Frac.eq(lfrac, rfrac)
    normalize(lfrac)
    normalize(rfrac)

    return lfrac.numerator==rfrac.numerator and lfrac.denominator == rfrac.denominator
end

function Frac.commonDenominator(lfrac, rfrac)
    normalize(lfrac)
    normalize(rfrac)

    local a = lfrac.numerator
    local b = lfrac.denominator

    local c = rfrac.numerator
    local d = rfrac.denominator
    
    local mul = lcm(b,d)

    a = a * mul / b
    c = c * mul / d

    return a, c, mul
end

function Frac.lt(lfrac, rfrac)
    local a, c, _ = Frac.commonDenominator(lfrac, rfrac)

    return a<c
end

function Frac.le(lfrac, rfrac)
    local a, c, _ = Frac.commonDenominator(lfrac, rfrac)

    return a<=c
end

function Frac.inverse(frac)
    return Frac.new(frac.denominator,frac.numerator);
end

function Frac.unm(frac)
    local a = -frac.numerator
    local b =  frac.denominator

    return Frac.new(a,b)
end


local function approximation(num)
    local len = tostring(num):len()-1
    return Frac.new(math.floor(num*(10^len)+0.5), 10^len)
end

function Frac.mathOp(lobj, robj, fun)
    if type(lobj) == "number" then
        local x, y = math.modf(lobj)
        lobj = Frac(x, 1)
        if y~=0 then
            lobj = lobj + approximation(math.abs(y))
        end
    elseif type(robj) == "number" then
        local x, y = math.modf(robj)
        robj = Frac(x, 1)
        if y~=0 then
            robj = robj + approximation(math.abs(y))
        end
    end
    return fun(lobj, robj)
end

function Frac.add(lfrac, rfrac)
    local a, c, mul = Frac.commonDenominator(lfrac, rfrac)

    return Frac.new(a+c, mul)
end

function Frac.sub(lfrac, rfrac)
    local c = -rfrac.numerator
    local d = rfrac.denominator

    return Frac.add(lfrac, Frac.new(c, d))
end

function Frac.mul(lfrac, rfrac)
    local a = lfrac.numerator
    local b = lfrac.denominator

    local c = rfrac.numerator
    local d = rfrac.denominator

    return Frac.new(a*c, b*d)
end

function Frac.div(lfrac, rfrac)
    return Frac.mul(lfrac, Frac.inverse(rfrac))
end

function Frac.pow(frac, exp)
    if type(exp) ~= "number" then
        error("Wrong argument for pow!\nExpected number.", 0)
    end

    if exp == 0 then
        return Frac.new(1,1)
    end

    local a = frac.numerator
    local b = frac.denominator
    
    if exp < 0 then
        a,b = b,a
        exp = -exp
    end

    return Frac.new(a^exp, b^exp)
end

local function bar(a)
    print("lol i'm private ")
end

function Frac.foo(b)
    print("...")
    bar(b)
    print("...")
end


return Frac