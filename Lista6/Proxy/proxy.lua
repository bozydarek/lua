local CVector = require("CVector")

local proxy = {}

local metatab = {
    __call = function (proxy, array)
        return proxy.new(array)
    end,
    __concat = function (objL, objR)
        return CVector.concat(objL, objR)
    end,
    __tostring = function (obj)
        return obj.cvector:tostring()
    end,
    __len = function (obj)
        return obj.cvector:size()
    end,
    __index = function (obj, key)
        if type(key) == "number" then
            return obj.cvector:at(key)
        else
            return CVector.__index[key](obj.cvector)
        end
    end,
    __newindex = function (obj, key, value)
        if type(value) == "function" then
            rawset(obj, key, value)
            return
        end
        if value.CV_size ~= nil then
           rawset(obj, "cvector", value)
           return
        end
        return obj.cvector:saveAt(key,value)
    end,
    }

setmetatable(proxy, metatab)

function proxy.new(array)
    local self = setmetatable({}, metatab)

    if array.cvector == nil then
        self.cvector = CVector(array)
    else
        self.cvector = CVector(array.cvector)
    end

    return self
end


return proxy