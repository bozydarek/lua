local CVector = {}
CVector.__index = CVector

local metatab = {
    __call = function (CVector, array)
        return CVector.new(array)
    end
}

setmetatable(CVector, metatab)

local function unpackAndCopyDataFromArray(array, cvector)
    local maks = 0
    for i,_ in pairs(array) do
        if type(i) == "number" then
            maks = math.max( maks, i )
        end
    end

    cvector.CV_size = maks

    for i=1,maks do
        cvector.CV_data[i-1] = array[i]
    end
end

function CVector.new(array)
    if type(array) ~= "table" then 
        error("Wrong constructor arguments!", 0) end

    local self = setmetatable({}, CVector)

    if array.CV_size == nil then 
        self.CV_data = {}
        self.CV_size = 0
        unpackAndCopyDataFromArray(array, self)
    else
        self.CV_data = array:copyData()
        self.CV_size = array.CV_size
    end

    return self
end

function CVector:copyData()
    local copy = {}
    for i=1, self.CV_size do
        copy[i-1] = self.CV_data[i-1]
    end
    return copy;
end

function CVector:printCV()
    for i=0,self.CV_size-1 do
        print(i, self.CV_data[i])
    end
end

function CVector:empty()
    return self.CV_size == 0
end

function CVector:size()
    return self.CV_size
end

function CVector:inScope(i)
    return (i >= 0) and (i < self.CV_size)
end

function CVector:at(i)
    if ~self:inScope(i) then
        error("Out of scope")
    end

    return self.CV_data[i]
end

function CVector:saveAt(i, x)
    if ~self:inScope(i) then
        error("Out of scope")
    end

    self.CV_data[i] = x
end

function CVector:clear()
    for i=0,self.CV_size-1 do
        self.CV_data[i] = nil
    end

    self.CV_size = 0;
end

function CVector:pop_back()
    if self:empty() then 
        error("Trying to pop from empty vector",0)
    end

    local tmp = self.CV_data[self.CV_size-1]
    self.CV_size = self.CV_size - 1;
    return tmp
end

function CVector:push_back( el )
    self.CV_data[self.CV_size] = el
    self.CV_size = self.CV_size + 1;
end

function CVector:erase(i, j)
    if j ~= nil then
        if not (self:inScope(i) and self:inScope(j)) then
            error("Out of scope")
        end

        if j < i then
            i,j = j,i
        end
        
        local range = j - i + 1
        for it=i,self.CV_size-1 do
            self.CV_data[it] = self.CV_data[it+range]
        end
        self.CV_size = self.CV_size-range
    else
        for it=i,self.CV_size-1 do
            self.CV_data[it] = self.CV_data[it+1]
        end
        self.CV_size = self.CV_size-1
    end

end

function CVector:insert(i, el)
    if type(el) == "table" then
        if el.CV_size == nil then 
            self:insert(i, CVector(el))
        else
            range = el.CV_size
            for it=self.CV_size-1, i, -1 do
                self.CV_data[it+range] = self.CV_data[it]
            end
            for it=0,range-1 do
                self.CV_data[it+i] = el.CV_data[it]
            end
            
            self.CV_size = self.CV_size+range       
        end
    else
        for it=self.CV_size-1, i, -1 do
            self.CV_data[it+1] = self.CV_data[it]
        end
        self.CV_data[i] = el
        self.CV_size = self.CV_size+1
    end
end

-- TODO: Przeciąż dla klasy wektora operację konkatenacji (łączącej ze sobą wektory), 
-- długości, indeksowania, tostring oraz iterator ipairs.

function CVector.concat(lvect, rvect)
    local result = CVector(lvect)
    for it=0,rvect.CV_size-1 do
        result.CV_data[it+result.CV_size-1] = rvect.CV_data[it]
    end
    return result
end

function CVector:tostring()
    io.write("< ")
    for i=0, self.CV_size-2 do
        io.write(tostring(self.CV_data[i])..", ")
    end 
    if self.CV_size > 0 then
        io.write(tostring(self.CV_data[self.CV_size-1]))
    end
    io.write(" >\n")
end

local function _ipairs(vect, var)
    var = var + 1
    local value = vect.CV_data[var] --[var]
    if var >= vect.CV_size then 
        return 
    end
    return var, value
end

function ipairs(vect) return _ipairs, vect, -1 end

return CVector