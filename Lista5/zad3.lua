package.path = package.path .. ";../Tools/?.lua"
require("tools")

function subsetsOf( tab )
    if #tab == 1 then
        return {{},tab}
    end

    local last = table.remove( tab )
    local subs = subsetsOf(tab)
    local subsSize = #subs
    for i=1, subsSize do
        local tmp = {table.unpack(subs[i])}
        table.insert( tmp, last )
        table.insert( subs, tmp )
    end

    return subs
end

-- function subsets( tab )
--     local subs = subsetsOf(tab)
--     return  function(state)
--                 state[1] = state[1] + 1
--                 return state[2][state[1]]
--             end,{0,subs}
-- end

print_tab_rec(subsetsOf({1,2,3}))
print_tab_rec(subsetsOf({1,2,'a',4}))

-- for s in subsets({ a = true , b = true , 3= true }) do
--     print ( table.concat ( s ))
-- end

