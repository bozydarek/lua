function zip( ... )
    local arg = {...}
    return  function(state)
                local result = {}
                for i=1,#state[3] do
                    if state[3][i][state[1]] == nil then
                        return nil
                    end
                    result[#result+1] = state[3][i][state[1]]
                end
                state[1] = state[1] + 1
                return table.unpack(result)

            end, {1, #arg, arg}
end


print(zip ({1 , 2},{ 'a' , 'b' , 'c' , 'd'}))
print(zip ({1 , 3, 8}, {'2', '2'}))

for x , y in zip ({ 'a' , 'b' , 'c' , 'd'} , {40 , 50 , 60}) do
    print (x , y )
end

for x , y in zip ({ 'a' } , {40 , 50 , 60}) do
    print (x , y )
end

for x , y in zip ({ 1, 2, 3, 4, 5, 6 } , {}) do
    print (x , y )
end