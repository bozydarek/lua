function chain1( ... )
    return  function(state)
                if #state < 1 then 
                    return nil
                end
                while #state[1] < 1 do
                    table.remove(state, 1 )
                    if #state < 1 then 
                        return nil
                    end
                end
                local ret_val = state[1][1]
                table.remove(state[1], 1 )
                return ret_val
            end, {...}
end


function chain( ... )
    local args = {...}
    local tabs = 1
    local ptr2elem = 1

    while #args[tabs] < 1 do
        tabs = tabs+1
        if tabs > #args then
            return nil
        end
    end

    return  function(state)
                if #state[3] < state[1] then 
                    return nil
                end
                while #(state[3][state[1]]) < state[2] do
                    state[1] = state[1] + 1
                    state[2] = 1
                    if #state[3] < state[1] then 
                        return nil
                    end
                end
                local ret_val = state[3][state[1]][state[2]]
                state[2] = state[2] + 1
                return ret_val
            end, {tabs, ptr2elem, args}
end

print(chain ({1 , 2}))
print(chain ({1 , 3, 5}, {'a'}))

for x in chain ({1 , 2}) do
    print ( x )
end

for x in chain ({ 'a ' , 'b ' , 'c '} , {40 , 50} , {} , {6 , 7}) do
    print ( x )
end

for x in chain ({ 'a ' , 'b ' , 'c '} , {},{},{}, {40 , "50!"} , {} , {"nana", "nana", "nana", "Batman!"}) do
    print ( x )
end

