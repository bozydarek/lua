function print_tab (tab)
    io.write "[ "
    for i=1, #tab-1 do
        io.write (tab[i])
        io.write ", "
    end
    if #tab~=0 then
        io.write (tab[#tab])
    end
    print (" ]")
    -- print (tab[#tab] .. " ]") -- ciekawe ze print wypisuje wartosci inaczej niz io.write 
end

function print_tab_s (tab)
    io.write "[ "
    for i=1, #tab-1 do
        io.write (tostring(tab[i]))
        io.write ", "
    end
        io.write (tostring(tab[#tab]))
    print (" ]")
end


function print_tab_better(tab)
    local indexes = {}
    for k , _ in pairs(tab) do
        if type(k)=='number' then
            indexes[#indexes+1] = k
        end
    end

    table.sort( indexes )
    last = indexes[1]
    tab_end = indexes[#indexes]

    io.write "[ "
    for _, v in ipairs(indexes) do
        if v > last+1 then
            for i=1,v-(last+1) do
                io.write "nil, "
            end
        end
        last = v

        io.write (tostring(tab[v]))

        if v ~= tab_end then
            io.write ", " 
        end
    end
    print (" ]")
end


function print_tab_rec (tab)
    print_tab_r(tab)
    print ""
end

function print_tab_r (tab)
    io.write "[ "
    for i=1, #tab-1 do
        if type(tab[i]) =='table' then
            print_tab_r(tab[i])
        else
            io.write (tab[i])
            io.write ", "
        end
    end
    if #tab~=0 then
        if type(tab[#tab]) =='table' then
            print_tab_r(tab[#tab])
        else
            io.write (tab[#tab])
        end
    end
    io.write (" ]")
end

function prime(n)
    if n<=1 then return false end
    if n==2 then return true end
    if n%2==0 then return false end

    for i = 3, n^(1/2), 2 do
        if (n % i) == 0 then
            return false
        end
    end
    return true
end

function sum_all ( ... )
    local tab = ...
    local out = 0
    if type(tab) =='table' then
        for k , v in pairs(tab) do
            if(type(k)=='number') then
                out = out + v 
            end
        end

    else
        for k , v in pairs { ... } do 
            if(type(k)=='number') then
                out = out + v 
            end
        end
    end
    return out
end
