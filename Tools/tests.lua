function run_tests( funct, function_name, test_array )

    print ("\27[1mTest "..function_name.." function:\27[0m")

    local passed_tests = 0
    for _,v in ipairs(test_array) do
        local res = funct(v[1])
        if res ~= v[2] then
            print("TEST: "..tostring(v[1]).." \27[31mfail\27[0m")
            print("Expected:\t"..tostring(v[2]))
            print("Actual:  \t"..tostring(res))
        else
            passed_tests = passed_tests + 1
        end
    end

    if passed_tests == #test_array then
        print ("\27[32m"..passed_tests.."/"..#test_array.." test passed\27[0m")
    else
        print ("\27[31m"..passed_tests.."/"..#test_array.." test passed\27[0m")
    end
end

function run_tests_from_array( funct, function_name, test_array, input_no, output_no )

    print ("\27[1mTest "..function_name.." function:\27[0m")

    local passed_tests = 0
    for _,v in ipairs(test_array) do
        local sub = {}
        table.move(v, 1, input_no, 1, sub)
        local ans = {}
        table.move(v, input_no+1, input_no+output_no, 1, ans)

        local res = {funct(table.unpack(sub))}
        local pass = true

        for i=1, output_no do
            if res[i] ~= ans[i] then
                pass = false;
            end
        end

        if pass == false then
            print("TEST: "..concat_args(table.unpack(sub)).." \27[31mfail\27[0m")
            print("Expected:\t"..concat_args(table.unpack(ans)))
            print("Actual:  \t"..concat_args(table.unpack(res)))
        else
            passed_tests = passed_tests + 1
        end
    end

    if passed_tests == #test_array then
        print ("\27[32m"..passed_tests.."/"..#test_array.." test passed\27[0m")
    else
        print ("\27[31m"..passed_tests.."/"..#test_array.." test passed\27[0m")
    end
end

function concat_args (...)
    local result = ""
    local arg={...}

    for _,v in ipairs(arg) do
        result = result .. tostring(v) .. ", "
    end

    result = result:sub(1, -3)

    return result
end



-- function f (a,b,c)
--     return a+b+c, a*b*c, (a+b+c)/3, "wat?"
-- end


-- tests_for_f={
--     {1,1,1,3,1,1.0,"wat?"},
--     {0,0,0,0,0,0.0},
--     {1,2,3,6,6,2.0,"wat?"},
--     {1,2,3,6,16,2.0,"wat?"}
-- }

-- run_tests_from_array(f,"f",tests_for_f, 3, 4)