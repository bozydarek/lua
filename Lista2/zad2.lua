package.path = package.path .. ";../Tools/?.lua"
require("tools")

function borders(tab)
    local out = {}
    local indexes = {}
    for k , _ in pairs(tab) do
        if type(k)=='number' then
            if k > 0 then
                indexes[#indexes+1] = k
            end
        end
    end

    table.sort( indexes )
    last = indexes[1]
    
    for _, v in ipairs(indexes) do
        if v > last+1 then
            out[#out+1] = last
        end
        last = v
    end
   if last ~= nil then
        out[#out+1] = last --add last numeric index on the end
    end
    return out
end


function seq_check ( ... )
    local tab = ...
    local out = nil
    if type(tab) =='table' then
        out = borders(tab)
    else
        out = borders({...})
    end
    if #out == 0 then 
        return true, {0}
    end
    return #out==1, out
end


function print_test(bool, tab)
    io.write(tostring(bool)..", ")
    print_tab(tab)
end

print_test(seq_check({1,2,3,4,5,6,7,8,9,10}))

print_test(seq_check({1,2,nil,4,nil,nil,8,nil,12,nil,3.14}))
print_test(seq_check(1,2,nil,3))
print_test(seq_check(1,nil,2,nil,3))
print_test(seq_check(nil,1,nil,2,nil,3))
print_test(seq_check(nil,nil,nil,1,nil,2,nil,3))

tab = {nil,1,nil,nil,2,3,4,nil,nil,8,9,10,nil}
print_test(seq_check(tab))
tab[666] = "alamakota"
print_test(seq_check(tab))

print_test(seq_check({}))

tab2 = {k=7, z=42}
print_test(seq_check(tab2))

