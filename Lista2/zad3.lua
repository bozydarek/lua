package.path = package.path .. ";../Tools/?.lua"
require("tools")

function moveinto(tab1, i1, j1, tab2, i2)
    for i=i1,j1 do
        table.insert( tab2, i2+i-i1, tab1[i] )
    end
end

--------------------------------------------------
tab1 = {1 , 2 , 3 , 4 , 5 , 6}
print_tab_better(tab1)

moveinto ({61 , 62 , 63 , 64 , 65} , 2 , 4 , tab1 , 4)
print_tab_better(tab1)

--------------------------------------------------
tab2 = {1 , nil , 3 , 77 , nil , 8}
print_tab_better(tab2)

moveinto ({3 , 4 , nil , 6 , 7} , 2 , 4 , tab2 , 4)
-- tab2 -- > {1, nil, 3, 4, nil, 6, 77, nil, 8}
print_tab_better(tab2) -- [ 1, nil, 3, 4, nil, 6, 77, 8 ]