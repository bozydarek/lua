#ifndef TOOLS_H
#define TOOLS_H

#include <iostream>
#include <lua.hpp>

#ifdef DEBUG
static void stackDump (lua_State *L, std::string separator = "\n")
{
    int top = lua_gettop(L);

    std::cout << "Stack contains " << top << (top == 1 ? " element\n" : " elements.\n");

    for (int i = 1; i <= top; i++) // odwiedzamy elementy od dołu
    {
        int t = lua_type(L, i);
        int number = i - top - 1;
        switch (t)
        {
            case LUA_TSTRING: // napis
                std::cout << number << ": " << lua_tostring(L, i) << separator;
                break;

            case LUA_TBOOLEAN: // wartość logiczna
                std::cout << number << ": " << (lua_toboolean(L, i) ? "true" : "false") << separator;
                break;

            case LUA_TNUMBER: // liczby
                if (lua_isinteger(L, i)) // integer
                    std::cout << number << ": " << lua_tointeger(L, i) << separator;
                else // float
                    std::cout << number << ": " << lua_tonumber(L, i) << separator;
                break;

            default: // pozostałe
                std::cout << number << ": " << lua_typename(L, t) << " # " << lua_topointer(L, i) << separator;
                break;
        }
    }
    std::cout << std::endl;
}

#endif

void print_error(lua_State *L, const char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    lua_close(L);
    
    puts("");    

    exit(-10);
}

#endif //TOOLS_H
