#include <math.h>
#include <lua.hpp>
#include <vector>

#ifdef DEBUG
#include "tools.h"
#endif

static int summation (lua_State *L)
{
    int elemsInStack = lua_gettop(L);
    double result = 0;

    for (int i = 1; i <= elemsInStack; ++i) {
        result += luaL_checknumber(L, i);
    }
	lua_pushnumber(L, result);
	return 1;
}

static int reduce (lua_State *L)
{
    int noOfParams = lua_gettop(L);
    if(not lua_isfunction(L,-noOfParams)){
        return luaL_error(L, "Wrong #1 parameter. Function require");
    }
    if(not lua_istable(L,-noOfParams+1)){
        return luaL_error(L, "Wrong #2 parameter. Table require");
    }
    if(noOfParams > 3){
        return luaL_error(L, "To many arguments!");
    }
    int len = (int)luaL_len(L, -noOfParams+1);

    if(len == 0){
        return luaL_error(L, "Table (#2 parameter) is empty!");
    }

    int idx = 1;
    if(noOfParams==2){
        lua_geti(L, -1, idx++);
    }

    for (; idx <= len; ) {
        lua_pushvalue(L, -3); // copy function on top
        lua_pushvalue(L, -2); // copy arg1 on top
        lua_remove(L, -3); // remove arg1, is there any function to move stack elem on top?
        lua_geti(L, -3, idx++); // take argument from table on index idx and increment idx
        int ret = lua_pcall(L,2,1,0); // call function with 2 args
        if(ret != LUA_OK){
            return luaL_error(L, "Pcall error");
        }
    }

    return 1;
}

static int reverse(lua_State *L)
{
    if(not lua_istable(L, -1)){
        return luaL_error(L, "Wrong #1 parameter");
    }

    int len = (int)luaL_len(L, -1);

#ifdef DEBUG
    std::cout << "Table len: " << len << "\n";
#endif

    for (int i = 0; i < len/2; ++i) {
        lua_pushinteger(L, len-i);
        lua_geti(L, -2, i+1);

        lua_pushinteger(L, i+1);
        lua_geti(L, -4, len-i);

        //lua_rotate(L,2,1);
        lua_settable(L,-5);
        lua_settable(L,-3);
    }

    return 1;
}

static int filter(lua_State *L)
{
    int noOfParams = lua_gettop(L);
    if(noOfParams > 2){
        return luaL_error(L, "To many arguments!");
    }
    if(not lua_isfunction(L,-2)){
        return luaL_error(L, "Wrong #1 parameter. Function require");
    }
    if(not lua_istable(L,-1)){
        return luaL_error(L, "Wrong #2 parameter. Table require");
    }
    int len = (int)luaL_len(L, -1);

    lua_createtable(L, len, 0);

    int idx = 0;
    for (int i = 0; i < len; ++i) {
        lua_pushvalue(L, -3); // copy function on top
        lua_geti(L, -3, i+1); // copy argument from table
        int ret = lua_pcall(L, 1, 1, 0); // call function with 1 arg
        if(ret != LUA_OK){
            return luaL_error(L, "Pcall error");
        }

        if (lua_type(L, -1) != LUA_TBOOLEAN){
            luaL_error(L, "Function (#1 parameter) should return boolean value.");
        }

        if(lua_toboolean(L, -1)){
            lua_geti(L, -3, i+1);
            lua_seti(L, -3, ++idx);
        }
        lua_pop(L, 1);

    }

    return 1;
}

static int splitAt(lua_State *L){
    int noOfParams = lua_gettop(L);

    std::vector<int> divisions;
    for (int i = 2; i <= noOfParams; ++i) {
        divisions.push_back((int)luaL_checkinteger(L, i));
    }
    lua_pop(L, noOfParams-1); // remove from stack

    if(not lua_istable(L,-1)){
        return luaL_error(L, "Wrong #1 parameter. Table require");
    }

    int length = (int)luaL_len(L, -1);
    int outs = 0; // no of outputs
    int idx = 0; // index in input table

#ifdef DEBUG
    std::cout << "divisions no: " << divisions.size() << "\n";
    for(auto e : divisions)
        std::cout << e << " ";
    std::cout << "\n";
#endif

    divisions.push_back(length); // simple trick which ensure that we will use all elements of input table

    for(int size : divisions){
        int len = std::min(size, length);
        lua_createtable(L,len,0);
        ++outs;

        for (int i = 1; i <= len; ++i) {
            lua_geti(L, -outs-1, idx + i);
            lua_seti(L, -2, i);
        }

        idx += len;
        length -= len;

        if(length == 0)
            break;
    }

    return outs;
}

static const struct luaL_Reg mylib [] =
{
	{"summation", summation},   // 1p
    {"reduce", reduce},         // 1p
    {"reverse", reverse},       // 1p
    {"filter", filter},         // 1p
    {"splitAt", splitAt},       // 2p
	{NULL, NULL}  // sentinel
};

extern "C" int luaopen_libMyLua(lua_State *L)  // wystawienie na zewnątrz (z extern C)
{
	luaL_newlib(L, mylib);
	return 1;
}
