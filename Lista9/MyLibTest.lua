package.path = package.path .. ";../Tools/?.lua"
require("tools")

local myLib = require'libMyLua'

print" --- Summation --- "
print(myLib.summation())
print(myLib.summation(1,2,3))
print(myLib.summation(0.1, 0.7, 0.2, 1))
print(myLib.summation(0.15, 0.234, 0.9876))
--print(myLib.summation(1,2,"kaczka",8))


print" --- Reduce --- "
local function f(x,y) return x+y end
local function f2(x,y) return x.." "..y end
local function f3(x,y) return {x=x["x"]+y["x"], y=x["y"]+y["y"]} end

print(myLib.reduce(f, {1}))
print(myLib.reduce(f, {1,2,3,4,5}))
print(myLib.reduce(f, {1,2,3,4,5}, 10))
--print(myLib.reduce(f, {1,2,3,4,5}, 10, 42))
print(myLib.reduce(f2, {"Ala", "ma", "kota"}, "Zdanie:"))
local res = myLib.reduce(f3, {{x=1,y=2},{x=2,y=1},{x=-2,y=-4}})
print("x="..res["x"],"y="..res["y"])


print" --- Reverse --- "
print_tab_s(myLib.reverse({1,2,3}))
print_tab_s(myLib.reverse({1,2,3,4}))
print_tab_s(myLib.reverse({1}))
print_tab_s(myLib.reverse({}))
--print(myLib.reverse("wat?"))
--print(myLib.reverse(42))
print_tab_s(myLib.reverse({"Ala", "ma", "kota"}))
print_tab_s(myLib.reverse({1, nil, "a", f}))
print_tab_rec(myLib.reverse({{1},{3,2}}))


print" --- Filter --- "
local function ff1(x) return x>0 end
local function ff2(x) return x:len() >= 2 and x:len() <= 4 end
local function ff3(x) return "banan" end

print_tab_s(myLib.filter(ff1, {1, -5, 3, -8, -11, 2, 42}))
print_tab_s(myLib.filter(ff2, {"Ala", "Kowalska", "ma", "bardzo", "rudego", "kota"}))
--print_tab_s(myLib.filter(ff3, {1, 2, 3, 4}))


print" --- SplitAt --- "
print_tab_rec({myLib.splitAt({1,2,3,4,5,6}, 2, 3)})
print_tab_rec({myLib.splitAt({1,2,3,4,5,6}, 5, 6, 7, 8)})
print_tab_rec({myLib.splitAt({"Ala", "Kowalska", "ma", "bardzo", "rudego", "kota"}, 1, 2, 1, 2)})
print_tab_rec({myLib.splitAt({}, 1, 2, 3)})
